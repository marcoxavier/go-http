package http

import (
	"net/http"
	"net/http/httptest"
	"testing"

	. "github.com/onsi/gomega" // test package
)

type testResponse struct {
	Key string `json:"key"`
}

func TestNewRestClient_WithAnInvalidHost(t *testing.T) {
	RegisterTestingT(t)

	restClient := NewClient("some_invalid_host", WithJSONDecoder())

	_, res, err := restClient.RequestData(http.MethodGet, "/api/1/test", nil, nil)

	Expect(err).To(HaveOccurred())
	Expect(res).To(BeNil())
}

func TestNewRestClient_WithAValidHost(t *testing.T) {
	RegisterTestingT(t)

	server, _ := createServer(http.StatusOK, []byte("{}"))
	defer server.Close()

	restClient := NewClient(server.URL, WithJSONDecoder())

	status, res, err := restClient.Request(http.MethodGet, "/api/1/test", nil, nil)

	Expect(err).ToNot(HaveOccurred())
	Expect(status).To(Equal(http.StatusOK))
	Expect(res).ToNot(BeNil())
}

func TestRestClient_AddDefaultHeader(t *testing.T) {
	RegisterTestingT(t)

	server, _ := createServer(http.StatusOK, []byte("{}"))
	defer server.Close()

	restClient := NewClient(server.URL,
		WithJSONDecoder(),
		WithClientDefaultHeader("Authorization", "FirstValue"),
		WithClientDefaultHeader("Authorization", "SecondValue"),
		WithClientDefaultHeader("Authorization", "LastValue"),
	)

	Expect(len(restClient.defaultHeaders)).To(Equal(1))
	Expect(restClient.defaultHeaders["Authorization"]).To(Equal("LastValue"))
}

func TestRestClient_RequestData_WithStatusOkAndData(t *testing.T) {
	RegisterTestingT(t)

	expectedResponse := []byte(`{"key": "value"}`)

	server, _ := createServer(http.StatusOK, expectedResponse)
	defer server.Close()

	restClient := NewClient(server.URL, WithJSONDecoder())

	status, res, err := restClient.RequestData(http.MethodGet, "/api/1/test", nil, nil)

	Expect(err).ToNot(HaveOccurred())
	Expect(status).To(Equal(http.StatusOK))
	Expect(res).ToNot(BeNil())
	Expect(len(res)).ToNot(Equal(0))
}

func TestRestClient_RequestData_WithStatusNotFound(t *testing.T) {
	RegisterTestingT(t)

	server, _ := createServer(http.StatusNotFound, nil)
	defer server.Close()

	restClient := NewClient(server.URL, WithJSONDecoder())

	status, res, err := restClient.RequestData(http.MethodGet, "/api/1/test", nil, nil)

	Expect(err).ToNot(HaveOccurred())
	Expect(status).To(Equal(http.StatusNotFound))
	Expect(res).ToNot(BeNil())
	Expect(len(res)).To(Equal(0))
}

func TestRestClient_Get_WithAValidStructure(t *testing.T) {
	RegisterTestingT(t)

	expectedResponse := []byte(`{"key": "value"}`)

	server, _ := createServer(http.StatusOK, expectedResponse)
	defer server.Close()

	restClient := NewClient(server.URL, WithJSONDecoder())

	var response testResponse
	status, err := restClient.Get("/api/1/test", nil, &response)

	Expect(err).ToNot(HaveOccurred())
	Expect(status).To(Equal(http.StatusOK))
	Expect(response.Key).To(Equal("value"))
}

func TestRestClient_Get_WithANilResponse(t *testing.T) {
	RegisterTestingT(t)

	expectedResponse := []byte(`{"key": "value"}`)

	server, _ := createServer(http.StatusOK, expectedResponse)
	defer server.Close()

	restClient := NewClient(server.URL, WithJSONDecoder())

	status, err := restClient.Get("/api/1/test", nil, nil)

	Expect(err).ToNot(HaveOccurred())
	Expect(status).To(Equal(http.StatusOK))
}

func TestRestClient_Get_WithAInvalidStructure(t *testing.T) {
	RegisterTestingT(t)

	expectedResponse := []byte(`{"key": "value"}`)

	server, _ := createServer(http.StatusOK, expectedResponse)
	defer server.Close()

	restClient := NewClient(server.URL, WithJSONDecoder())

	response := struct {
		Key string `json:"invalidJsonKey"`
	}{}
	status, err := restClient.Get("/api/1/test", nil, &response)

	Expect(err).ToNot(HaveOccurred())
	Expect(status).To(Equal(http.StatusOK))
	Expect(response.Key).ToNot(Equal("value"))
}

func TestRestClient_Post_WithAValidStructure(t *testing.T) {
	RegisterTestingT(t)

	expectedResponse := []byte(`{"key": "value"}`)

	server, _ := createServer(http.StatusOK, expectedResponse)
	defer server.Close()

	restClient := NewClient(server.URL, WithJSONDecoder())

	var response testResponse
	status, err := restClient.Post("/api/1/test", nil, expectedResponse, &response)

	Expect(err).ToNot(HaveOccurred())
	Expect(status).To(Equal(http.StatusOK))
	Expect(response.Key).To(Equal("value"))
}

func TestRestClient_Post_WithANilResponse(t *testing.T) {
	RegisterTestingT(t)

	expectedResponse := []byte(`{"key": "value"}`)

	server, _ := createServer(http.StatusOK, expectedResponse)
	defer server.Close()

	restClient := NewClient(server.URL, WithJSONDecoder())

	status, err := restClient.Post("/api/1/test", nil, expectedResponse, nil)

	Expect(err).ToNot(HaveOccurred())
	Expect(status).To(Equal(http.StatusOK))
}

func TestRestClient_Post_WithAInvalidStructure(t *testing.T) {
	RegisterTestingT(t)

	expectedResponse := []byte(`{"key": "value"}`)

	server, _ := createServer(http.StatusOK, expectedResponse)
	defer server.Close()

	restClient := NewClient(server.URL, WithJSONDecoder())

	response := struct {
		Key string `json:"invalidJsonKey"`
	}{}
	status, err := restClient.Post("/api/1/test", nil, expectedResponse, &response)

	Expect(err).ToNot(HaveOccurred())
	Expect(status).To(Equal(http.StatusOK))
	Expect(response.Key).ToNot(Equal("value"))
}

func TestRestClient_Put_WithAValidStructure(t *testing.T) {
	RegisterTestingT(t)

	expectedResponse := []byte(`{"key": "value"}`)

	server, _ := createServer(http.StatusOK, expectedResponse)
	defer server.Close()

	restClient := NewClient(server.URL, WithJSONDecoder())

	var response testResponse
	status, err := restClient.Put("/api/1/test", nil, expectedResponse, &response)

	Expect(err).ToNot(HaveOccurred())
	Expect(status).To(Equal(http.StatusOK))
	Expect(response.Key).To(Equal("value"))
}

func TestRestClient_Put_WithANilResponse(t *testing.T) {
	RegisterTestingT(t)

	expectedResponse := []byte(`{"key": "value"}`)

	server, _ := createServer(http.StatusOK, expectedResponse)
	defer server.Close()

	restClient := NewClient(server.URL, WithJSONDecoder())

	status, err := restClient.Put("/api/1/test", nil, expectedResponse, nil)

	Expect(err).ToNot(HaveOccurred())
	Expect(status).To(Equal(http.StatusOK))
}

func TestRestClient_Put_WithAInvalidStructure(t *testing.T) {
	RegisterTestingT(t)

	expectedResponse := []byte(`{"key": "value"}`)

	server, _ := createServer(http.StatusOK, expectedResponse)
	defer server.Close()

	restClient := NewClient(server.URL, WithJSONDecoder())

	response := struct {
		Key string `json:"invalidJsonKey"`
	}{}
	status, err := restClient.Put("/api/1/test", nil, expectedResponse, &response)

	Expect(err).ToNot(HaveOccurred())
	Expect(status).To(Equal(http.StatusOK))
	Expect(response.Key).ToNot(Equal("value"))
}

func createServer(statusCode int, body []byte) (*httptest.Server, func() *http.Request) {
	var request *http.Request

	fn := func() *http.Request {
		return request
	}

	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		request = r

		w.WriteHeader(statusCode)
		w.Write(body)
	}))

	return server, fn
}
