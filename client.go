package http

import (
	"bytes"
	"io/ioutil"
	"net/http"

	"time"

	"go-http/decoder"

	"net"

	"github.com/pkg/errors"
)

// ClientOption is a optional parameter.
type ClientOption func(client *Client)

// Decoder is capable to parse the response into body
type Decoder interface {
	Decode(response *http.Response, body interface{}) error
	Encode(request interface{}) ([]byte, error)
}

// WithJSONDecoder allows setting default decoder as json.
func WithJSONDecoder() ClientOption {
	return func(c *Client) {
		c.decoder = decoder.JSONDecoder{}
	}
}

// WithDecoder allows setting default decoder.
func WithDecoder(decoder Decoder) ClientOption {
	return func(c *Client) {
		c.decoder = decoder
	}
}

// WithClientDefaultHeader allows setting http headers.
func WithClientDefaultHeader(key, value string) ClientOption {
	return func(c *Client) {
		c.defaultHeaders[key] = value
	}
}

// WithTimeout allows setting http headers.
func WithTimeout(timeout time.Duration) ClientOption {
	return func(c *Client) {
		c.client.Timeout = timeout
	}
}

// Client flows traffic between services.
type Client struct {
	client         http.Client
	host           string
	defaultHeaders map[string]string
	decoder        Decoder
}

// NewClient creates a new http client for a specific host.
// It uses JSON as default decoder, you can override it with WithDecoder function
func NewClient(host string, opts ...ClientOption) *Client {
	c := &Client{
		client: http.Client{
			Transport: &http.Transport{
				Proxy: http.ProxyFromEnvironment,
				DialContext: (&net.Dialer{
					Timeout:   30 * time.Second,
					KeepAlive: 30 * time.Second,
					DualStack: true,
				}).DialContext,
				MaxIdleConns:          50,
				IdleConnTimeout:       60 * time.Second,
				TLSHandshakeTimeout:   10 * time.Second,
				ExpectContinueTimeout: 1 * time.Second,
			},
		},
		host:           host,
		decoder:        decoder.JSONDecoder{},
		defaultHeaders: make(map[string]string),
	}

	c.Reconfigure(opts...)

	return c
}

// Reconfigure sets all optional options.
func (c *Client) Reconfigure(options ...ClientOption) {
	for _, option := range options {
		option(c)
	}
}

// Post creates a http request and submit with POST method.
// It also marshal the request body.
func (c *Client) Post(endpoint string, headers map[string]string, request interface{}, response interface{}) (int, error) {
	rawBody, err := c.decoder.Encode(request)
	if err != nil {
		return 0, errors.Wrap(err, "invalid request body")
	}

	res, err := c.request(http.MethodPost, endpoint, headers, rawBody)
	if err != nil {
		return 0, err
	}

	defer res.Body.Close()

	if err := c.decoder.Decode(res, response); err != nil {
		return 0, err
	}

	return res.StatusCode, nil
}

// Get creates a http request and submit with GET method
// It also marshal the request body
func (c *Client) Get(endpoint string, headers map[string]string, response interface{}) (int, error) {
	res, err := c.request(http.MethodGet, endpoint, headers, nil)
	if err != nil {
		return 0, err
	}

	defer res.Body.Close()

	if err := c.decoder.Decode(res, response); err != nil {
		return 0, err
	}

	return res.StatusCode, nil
}

// Put creates a http request and submit with PUT method.
// It also marshal the request body.
func (c *Client) Put(endpoint string, headers map[string]string, request interface{}, response interface{}) (int, error) {
	rawBody, err := c.decoder.Encode(request)
	if err != nil {
		return 0, errors.Wrap(err, "invalid request body")
	}

	res, err := c.request(http.MethodPut, endpoint, headers, rawBody)
	if err != nil {
		return 0, err
	}

	defer res.Body.Close()

	if err := c.decoder.Decode(res, response); err != nil {
		return 0, err
	}

	return res.StatusCode, nil
}

// Delete creates a http request and submit with DELETE method.
// It also marshal the request body.
func (c *Client) Delete(endpoint string, headers map[string]string, request interface{}, response interface{}) (int, error) {
	rawBody, err := c.decoder.Encode(request)
	if err != nil {
		return 0, errors.Wrap(err, "invalid request body")
	}

	res, err := c.request(http.MethodDelete, endpoint, headers, rawBody)
	if err != nil {
		return 0, err
	}

	defer res.Body.Close()

	if err := c.decoder.Decode(res, response); err != nil {
		return 0, err
	}

	return res.StatusCode, nil
}

// Request creates a http request and submit.
// It also marshal the request request.
func (c *Client) Request(method string, endpoint string, headers map[string]string, request interface{}) (int, []byte, error) {
	rawBody, err := c.decoder.Encode(request)
	if err != nil {
		return 0, nil, errors.Wrap(err, "invalid request body")
	}

	return c.RequestData(method, endpoint, headers, rawBody)
}

// RequestData creates a http request and submit.
// It submits with http body sent as parameters as is.
func (c *Client) RequestData(method string, endpoint string, headers map[string]string, rawRequest []byte) (int, []byte, error) {
	res, err := c.request(method, endpoint, headers, rawRequest)
	if err != nil {
		return 0, nil, err
	}

	defer res.Body.Close()

	output, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return res.StatusCode, nil, errors.Wrap(err, "error reading http body")
	}

	return res.StatusCode, output, nil
}

func (c *Client) request(method string, endpoint string, headers map[string]string, rawRequest []byte) (*http.Response, error) {
	url := c.host + endpoint

	req, err := http.NewRequest(method, url, bytes.NewBuffer(rawRequest))
	if err != nil {
		return nil, errors.Wrap(err, "error creating request")
	}

	for key, value := range c.defaultHeaders {
		req.Header.Add(key, value)
	}

	for key, value := range headers {
		req.Header.Add(key, value)
	}

	response, err := c.client.Do(req)
	if err != nil {
		return nil, errors.Wrap(err, "error running request")
	}

	return response, nil
}
