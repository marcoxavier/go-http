package decoder

import (
	"encoding/json"
	"net/http"

	"github.com/pkg/errors"
)

// JSONDecoder is a decoder of JSON
type JSONDecoder struct {
}

// Decode reads the JSON response and stores it in body.
func (d JSONDecoder) Decode(response *http.Response, body interface{}) error {
	if body == nil {
		return nil
	}

	if err := json.NewDecoder(response.Body).Decode(body); err != nil {
		return errors.Wrap(err, "failed to decode response")
	}

	return nil
}

// Encode writes JSON values to an output stream.
func (d JSONDecoder) Encode(request interface{}) ([]byte, error) {
	rawBody, err := json.Marshal(request)
	if err != nil {
		return nil, errors.Wrap(err, "invalid request body")
	}

	return rawBody, nil
}
